import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "../screens/Home";
import SecondScreen from "../screens/SecondScreen";
import Login from "../screens/Login";
import StudentInsert from "../screens/StudentInsert";
import HomeUser from "../screens/HomeUser";
import MyList from "../screens/MyList";
import ScreenTest from "../screens/ScreenTest"; 
import SearchScreen from "../screens/SearchScreen";

const MainStack = createNativeStackNavigator();

const Main = () => {
  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <MainStack.Screen name="Home" component={Home} />
      <MainStack.Screen name="SecondScreen" component={SecondScreen} />
      <MainStack.Screen name="Login" component={Login} />
      <MainStack.Screen name="HomeUser" component={HomeUser} />
      <MainStack.Screen name="StudentInsert" component={StudentInsert} />
      <MainStack.Screen name="MyList" component={MyList} />
      <MainStack.Screen name="ScreenTest" component={ScreenTest} />
      <MainStack.Screen name="SearchScreen" component={SearchScreen} />
    </MainStack.Navigator>
  );
};

export default () => {
  return (
    <NavigationContainer>
      <Main />
    </NavigationContainer>
  );
};
