import React, { Component } from 'react';
import { View, Text,Button, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';



class ScreenTest extends Component {


    constructor(props)
    {
        super(props);
    }




  render() {
    // Accéder aux paramètres de la navigation
    const { navigation } = this.props;
    const name = navigation.getParam('text', 'default name');


    return (
      <View>
        <Text>Je suis l'écran 2</Text>
        <Text>Nom : {name}</Text>
      </View>
    ); 
  }
}

export default ScreenTest;

