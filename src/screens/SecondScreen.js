import React, { Component } from 'react';
import { View, Text,Button, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';


export default class SignUp extends Component
{


    constructor(props)
    {
        super(props);
        this.state = {username:'', password:'', email:''};
    }

    InsertRecord = () =>
    {
        var username = this.state.username;
        var password = this.state.password;
        var email = this.state.email;

        if (username.length == 0 || password.length == 0 || email.length == 0 ) 
        {
            alert("Required field is missing!");
        }
        else
        {
            var InsertAPIURL = "http://192.168.0.115:80/api/SignUp.php";

            var headers = {
                'Accept':'application/json',
                'Content-Type':'application/json'
            };
            
            var Data = {
                username:username,
                password:password,
                email:email
            };

            fetch(InsertAPIURL,
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(Data)

                }
                )
                .then((response) => response.json())
                .then((responseJson) => 
                {
                    //console.log(responseJson);
                    alert(responseJson[0].Message);
                })
                .catch((error)=> 
                {
                    //console.error(error);
                    alert("Error: " + error);
                })

        }
    }



    render()
    {
        return(
           
                
    <View style={styles.ViewStyle}>
                    <TextInput
                        placeholder={"Username"}
                        placeholderTextColor={"#FF0000"}
                        style = {styles.txtStyle}
                        onChangeText={username => this.setState({username})}
                    />

                    <TextInput
                        placeholder={"Password"}
                        placeholderTextColor={"#FF0000"}
                        style = {styles.txtStyle}
                        onChangeText={password => this.setState({password})}

                    />

                    <TextInput
                        placeholder={"Email"}
                        placeholderTextColor={"#FF0000"}
                        style = {styles.txtStyle}
                        onChangeText={email => this.setState({email})}

                    />

                    <Button
                        title={"Insert"}
                        onPress = {this.InsertRecord}

                    />
           
            </View>       
     

        )
    }
}


const styles = StyleSheet.create({

    ViewStyle:
    {
        flex:1, 
        padding:20, 
        marginTop:10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:300,
        height:'50%'
 
    },

    txtStyle:
    {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2, 
        borderheight: 2, 
        borderColor: 'blue',
        width:'80%',
        height:'5%',

        borderBottomWidth:2,
        borderBottomColor:'blue',
        marginBottom:20

    }


});