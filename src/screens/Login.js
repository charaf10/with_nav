import React, { Component } from 'react';
import { View, Text,Button, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import Session from 'react-session-api'
import { useRoute } from "@react-navigation/native"


//class Login extends Component
class Login extends Component
{
 
    constructor(props)
    {
        super(props);
        this.state = {username:'', password:''};
    }

    InsertRecord = () =>
    {
        var Username = this.state.username;
        var Password = this.state.password;


        //console.log(Username);

        if (Username.length == 0 || Password.length == 0) 
        {
            alert("Required field is missing!");
        }
        else
        {
            var LoginAPIURL = "http://192.168.0.115:80/api/Login.php";

            var headers = {
                'Accept':'application/json',
                'Content-Type':'application/json'
            };
            
            var Data = {
                Username:Username,
                Password:Password
            };

            fetch(LoginAPIURL,
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(Data)

                }
                )
                .then((response) => response.json())
                .then((responseJson) => 
                {
                    console.log(responseJson[0].Message);
                    var numero = responseJson[1].MessageId;
                    console.log(numero);

                    if (responseJson[0].Message == "Logged In Successfully") {
                       this.props.navigation.navigate('HomeUser');
                
                    }
                })
                .catch((error)=> 
                {
                    console.error(error);
                    alert("Error: " + error);
                })

        }
    }



    render()
    {
        return(
           
                
    <View style={styles.ViewStyle}>
          <TextInput
            placeholder={"Username"}
            style = {styles.txtStyle}
            onChangeText={username => this.setState({username})}
          />

          <TextInput
            placeholder={"Password"}
            style = {styles.txtStyle}
            onChangeText={password => this.setState({password})}
          />

          <Button
            title={"Connect"}
            onPress = {this.InsertRecord}
          />




     </View>       
     
        )
    }
}


export default withNavigation(Login);
/*
export function redirect({navigation}) {
    return <Login />;
  }
*/



const styles = StyleSheet.create({

    ViewStyle:
    {
        flex:1, 
        padding:20, 
        marginTop:10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:300,
        height:'50%'
 
    },

    txtStyle:
    {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2, 
        borderheight: 2, 
        borderColor: 'blue',
        width:'80%',
        height:'5%',

        borderBottomWidth:2,
        borderBottomColor:'blue',
        marginBottom:20

    }
});


