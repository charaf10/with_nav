import React from 'react';
import { View, Text, FlatList, StyleSheet } from 'react-native';
import axios from 'axios';
import { useRoute } from "@react-navigation/native"



const styles = StyleSheet.create({
  card: {
    margin: 10,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOpacity: 0.3,
    shadowRadius: 2,
    shadowOffset: { width: 0, height: 2 },
  },
});  

class MyApp extends React.Component {
  state = {
    list: [],
  };

  componentDidMount() {
    axios.get('http://192.168.0.115:80/api/getWishlist.php')
      .then((response) => {
        this.setState({ list: response.data });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  render() {
    return (
      <FlatList style={{margin:45}}
        data={this.state.list}
        renderItem={({ item }) => (
          <View style={styles.card}>
            <Text>{item.id}</Text>
            <Text>{item.name}</Text>
            <Text>{item.description}</Text>
          </View>
        )}
        keyExtractor={(item) => item.id}
      />
    );
  }
}

export default MyApp;
