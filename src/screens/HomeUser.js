import React from "react";
import { View, Linking, StyleSheet, FlatList } from "react-native";
import {
  Layout,
  TopNav,
  Text,
  Button,
  Section,
  SectionContent,
  useTheme,
  themeColor,
} from "react-native-rapi-ui";
import { Ionicons } from "@expo/vector-icons";
import { component } from "react-native-rapi-ui/constants/colors";
import { withNavigation } from 'react-navigation';

 function HomeUser ({navigation})

 {

    const { isDarkmode, setTheme } = useTheme();



  return (




    <Layout>
      <TopNav
        middleContent="Home"
        rightContent={
          <Ionicons
            name={isDarkmode ? "sunny" : "moon"}
            size={20}
            color={isDarkmode ? themeColor.white100 : themeColor.dark}
          />
        }
        rightAction={() => {
          if (isDarkmode) {
            setTheme("light");
          } else {
            setTheme("dark");
          }
        }}
      />
    
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Section>
          <SectionContent >

            
            <Text fontWeight="bold" style={{ textAlign: "center", width:250 }}>
              {
              //key 
              }
            </Text>
            

        
          <Button
            text="New wishlist"
            onPress={() => {
              navigation.navigate("Login");
              
            }}
            style={{
              marginTop: 10,
            }}
          />
            <Button
              text="My wishlist"
              onPress={() => {
                navigation.navigate("MyList");
              }}
              style={{
                marginTop: 10,
              }}
            />

          <Button
            text="Search wishlist"
            onPress={() => {
              navigation.navigate("SearchScreen");
            }}
            style={{
              marginTop: 10,
            }}
          />
          </SectionContent>
        </Section>
      </View>
        

    </Layout>
  );


}

export default HomeUser;





const styles = StyleSheet.create({
    card: {
      margin: 10,
      padding: 10,
      color:'red',
      backgroundColor: '#fff',
      borderRadius: 5,
      shadowColor: '#000',
      shadowOpacity: 0.3,
      shadowRadius: 2,
      shadowOffset: { width: 0, height: 2 },
    },
  });
