import React, { Component } from 'react';
import { View, Text,Button, StyleSheet } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';


export default class StudentInsert extends Component
{


    constructor(props)
    {
        super(props);
        this.state = {RollNo:'', StudentName:'', Course:''};
    }

    InsertRecord = () =>
    {
        var RollNo = this.state.RollNo;
        var StudentName = this.state.StudentName;
        var Course = this.state.Course;

        if (RollNo.length == 0 || StudentName.length == 0 || Course.length == 0 ) 
        {
            alert("Required field is missing!");
        }
        else
        {
            var InsertAPIURL = "http://192.168.0.115:80/api/insert.php";

            var headers = {
                'Accept':'application/json',
                'Content-Type':'application/json'
            };
            
            var Data = {
                RollNo:RollNo,
                StudentName:StudentName,
                Course:Course
            };

            fetch(InsertAPIURL,
                {
                    method: 'POST',
                    headers: headers,
                    body: JSON.stringify(Data)

                }
                )
                .then((response) => response.json())
                .then((responseJson) => 
                {
                    //console.log(responseJson);
                    alert(responseJson[0].Message);
                })
                .catch((error)=> 
                {
                    //console.error(error);
                    alert("Error: " + error);
                })

        }
    }



    render()
    {
        return(
           
                
    <View style={styles.ViewStyle}>
                    <TextInput
                        placeholder={"RollNo"}
                        placeholderTextColor={"#FF0000"}
                        keyboardType={"numeric"}
                        style = {styles.txtStyle}
                        onChangeText={RollNo => this.setState({RollNo})}
                    />

                    <TextInput
                        placeholder={"Student Name"}
                        placeholderTextColor={"#FF0000"}
                        style = {styles.txtStyle}
                        onChangeText={StudentName => this.setState({StudentName})}

                    />

                    <TextInput
                        placeholder={"Course"}
                        placeholderTextColor={"#FF0000"}
                        style = {styles.txtStyle}
                        onChangeText={Course => this.setState({Course})}

                    />

                    <Button
                        title={"Save record"}
                        onPress = {this.InsertRecord}

                    />
           
            </View>       
     

        )
    }
}


const styles = StyleSheet.create({

    ViewStyle:
    {
        flex:1, 
        padding:20, 
        marginTop:10,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        width:300,
        height:'50%'
 
    },

    txtStyle:
    {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 2, 
        borderheight: 2, 
        borderColor: 'blue',
        width:'80%',
        height:'5%',

        borderBottomWidth:2,
        borderBottomColor:'blue',
        marginBottom:20

    }


});