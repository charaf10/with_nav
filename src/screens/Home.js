import React from "react";
import { View, Linking } from "react-native";
import {
  Layout,
  TopNav,
  Text,
  Button,
  Section,
  SectionContent,
  useTheme,
  themeColor,
} from "react-native-rapi-ui";
import { Ionicons } from "@expo/vector-icons";

export default function ({ navigation }) {


  const { isDarkmode, setTheme } = useTheme();
  return (
    <Layout>
      <TopNav
        middleContent="Home"
        rightContent={
          <Ionicons
            name={isDarkmode ? "sunny" : "moon"}
            size={20}
            color={isDarkmode ? themeColor.white100 : themeColor.dark}
          />
        }
        rightAction={() => {
          if (isDarkmode) {
            setTheme("light");
          } else {
            setTheme("dark");
          }
        }}
      />
      <View
        style={{
          flex: 1,
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Section>
          <SectionContent style={{ width: 350}}>
            <Text fontWeight="bold" style={{ textAlign: "center" }}>
              Bienvenue dans wishlist
            </Text>
            

            <Button
            text="Login"
            onPress={() => {
              navigation.navigate("Login");
            }}
            style={{
              marginTop: 10,
            }}
          />

            <Button
              text="Sign up!"
              onPress={() => {
                navigation.navigate("SecondScreen");
              }}
              style={{
                marginTop: 10,
              }}
            />   
                       
            <Button
              text="Screentest"
              onPress={() => {
               
                
                navigation.navigate('ScreenTest' , {text : 'heytest'});



              }}
              style={{
                marginTop: 10,
              }}
            />             


          </SectionContent>
        </Section>
      </View>
    </Layout>
  );
}



/*
            <Button
              style={{ marginTop: 10 }}
              text="Test link out"
              status="info"
              onPress={() => Linking.openURL("www.google.com")}
            />

*/